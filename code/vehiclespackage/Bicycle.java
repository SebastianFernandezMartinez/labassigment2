package vehiclespackage;

public class Bicycle {
    private String manufacturer;
    private int numberGears;
    private double maxSpeed;

    public String getManucturer(){
        return this.manufacturer;
    }
    
    public int getNumberGears(){
        return this.numberGears;
    }

    public double getMaxSpeed(){
        return this.maxSpeed;
    }

    public Bicycle(String manufacturer, int numberGears, double maxSpeed){
        this.manufacturer = manufacturer;
        this.numberGears = numberGears;
        this.maxSpeed = maxSpeed;
    }

    public String toString(){
        return "Manufacturer is: "+this.manufacturer+", numberOfGears are: "+this.numberGears+", maxSpeed is: "+this.maxSpeed;
    }
}
