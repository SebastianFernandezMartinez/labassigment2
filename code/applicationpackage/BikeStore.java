package applicationpackage;

import vehiclespackage.Bicycle;

public class BikeStore {
    public static void main(String[] args){
        Bicycle[] Bicycles = new Bicycle[4];
        Bicycles[0] = new Bicycle("trek", 6, 50.1);
        Bicycles[1] = new Bicycle("cannondale", 7, 50.2);
        Bicycles[2] = new Bicycle("specialized", 8, 50.2);
        Bicycles[3] = new Bicycle("cervelo", 9, 50.3);

        for(int i = 0;i<Bicycles.length;i++){
            System.out.println(Bicycles[i]);
        }
    }
}